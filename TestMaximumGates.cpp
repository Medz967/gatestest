#include "MaximumGates.h"

#include <iostream>
using std::cout;
using std::endl;

#include <vector>
using std::vector;

void test(vector<int> arrives, vector<int> departs, int gates);

int main() {
    int retval = 0;

	test({ 360, 400, 700 }, { 900, 515, 780 }, 2);

	test({ 360, 400, 700 }, { 900, 720, 780 }, 3);

	test({}, {}, 0);

	test({ 100 }, { 200 }, 1);

	test({ 100, 200, 300, 350, 600 }, { 150, 345, 340, 700, 610 }, 2);

	test({ 100, 200, 300, 400, 500, 600 }, {150, 250, 350, 450, 550, 650}, 1);

	test({ 100, 200, 300, 400, 500, 600 }, {610, 615, 620, 625, 630, 635}, 6);

	test({ 100, 200, 300, 400, 500, 600 }, {700, 250, 650, 420, 680, 610}, 4);

	test({ 100, 200, 300, 400, 500, 600 }, { 150, 350, 355, 445, 620, 610 }, 2);

    return 0;
}

void test(vector<int> arrives, vector<int> departs, int gates) {

	int maxGates = maximumGates(arrives, departs);

	if (maxGates == gates) {
		cout << "Pass: " << gates << " gates needed\n";
	}
	else {
		cout << "Fail: " << gates << " gates should be needed, but maximumGates returned " << maxGates << endl;
		
	}
}
